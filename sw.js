const staticCacheName = "site-static-v4";
const dynamicCache = "site-dynamic-v6";

const assets = [
  "/",
  "/index.html",
  "/js/app.js",
  "/js/ui.js",
  "/js/materialize.min.js",
  "/css/styles.css",
  "/css/materialize.min.css",
  "/img/dish.png",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v103/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
  "/pages/fallback.html",
];

const dynamicCacheSizeLimit = 15;

// limiting cache size

const limitCacheSize = (name, size) => {
  // opening caches
  console.log("caches", caches);
  caches.open(name).then((cache) => {
    // console.log("cache", cache);
    // cache get from open method
    // get key from cache that open
    cache.keys().then((keys) => {
      // console.log("keys", keys);
      // check if lengh of the key is bigger that size that specified/allowed
      // if length of the key is bigger, then delete the first key of cache
      // if that
      if (keys.length > size) {
        cache.delete(keys[0]).then(limitCacheSize(name, size));
      }
    });
  });
};

// install event
self.addEventListener("install", (evt) => {
  //   console.log("service worker has been installed");

  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      console.log("caching shell assets");
      cache.addAll(assets);
    })
  );
});

// active event
self.addEventListener("activate", (evt) => {
  //   console.log("service worker has been activated");
  evt.waitUntil(
    caches.keys().then((keys) => {
      return Promise.all(keys.filter((key) => key !== staticCacheName).map((key) => caches.delete(key)));
    })
  );
});

// fetch event
self.addEventListener("fetch", (evt) => {
  //   console.log("fetch event", evt);

  if (evt.request.url.indexOf("firestore.googleapis.com") === -1) {
    evt.respondWith(
      caches.match(evt.request).then((cacheRes) => {
        return (
          cacheRes ||
          fetch(evt.request)
            .then((fetchRes) => {
              return caches.open(dynamicCache).then((cache) => {
                cache.put(evt.request.url, fetchRes.clone());
                limitCacheSize(dynamicCache, dynamicCacheSizeLimit);
                return fetchRes;
              });
            })
            .catch(() => {
              if (evt.request.url.indexOf(".html") > -1) {
                return caches.match("/pages/fallback.html");
              }
            })
        );
      })
    );
  }
});
